var app = angular.module('app', [], function() {} );

app.directive('match', function($parse) {
  return {
    require: 'ngModel',
    link: function(scope, elem, attrs, ctrl) {
      scope.$watch(function() {        
        return $parse(attrs.match)(scope) === ctrl.$modelValue;
      }, function(currentValue) {
        ctrl.$setValidity('mismatch', currentValue);
      });
    }
  };
});

app.controller('FormController', function ($scope) {
  $scope.fields = {
    email: '',
    emailConfirm: ''
  };
  
  $scope.submit = function() {
    alert("Submit!");
  };
})

